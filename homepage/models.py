from django.db import models

# Create your models here.


class Subject(models.Model):
    matkul = models.CharField('Mata Kuliah', max_length=120, null=True)
    dosen = models.CharField('Dosen', max_length=120, null=True)
    sks = models.IntegerField('Jumlah SKS', null=True)
    deskripsi = models.TextField('Keterangan', blank=True)
    semester = models.CharField(
        'Semester', max_length=120, null=True, help_text='Contoh: 1')
    kelas = models.CharField('Kelas', max_length=120, null=True)
