from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('tambahjadwal/', views.tambah, name='tambah'),
    path('detail/<int:index>/', views.detail, name='detail'),
    path('cekjadwal/', views.jadwal, name='jadwal'),

]
