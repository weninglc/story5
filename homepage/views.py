from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Subject
from .forms import SubjectForm


# Create your views here.


def index(request):
    return render(request, 'index.html')


def tambah(request):
    submitted = False
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            # return render(request, 'tambah.html')
            return HttpResponseRedirect('/tambahjadwal/?submitted=True')
    else:
        form = SubjectForm()
        if 'submitted' in request.GET:
            submitted = True
    return render(request, 'tambah.html', {'form': form, 'submitted': submitted})


def detail(request, index):
    subject = Subject.objects.get(pk=index)
    return render(request, 'detail.html', {'subject': subject})


def jadwal(request):
    if request.method == "POST":
        print("LOHLAHLOH")
        Subject.objects.get(id=request.POST['id']).delete()
        return redirect('/cekjadwal/')
    list_subject = Subject.objects.all()
    return render(request, 'jadwal.html', {'subject_list': list_subject})
