from django.contrib import admin
from .models import Subject

# Register your models here.
@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('matkul', 'dosen', 'sks', 'semester', 'kelas', 'deskripsi')
    ordering = ('semester', 'matkul', 'dosen')
    search_fields = ('matkul', 'dosen', 'semester')
