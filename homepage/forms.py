from django.forms import ModelForm
from .models import Subject


class SubjectForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Subject
        fields = '__all__'
